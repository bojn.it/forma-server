import type { FormaField } from '@bojnit/forma-common'
import type { FormaSchema } from '@bojnit/forma-common'
import type { Metadata } from '@bojnit/forma-common/internal/lib'
import { getMetadataFor } from '@bojnit/forma-common/internal/lib'
import type { Newable } from 'ts-essentials'

export async function getFormaSchemaOf<T = unknown>(
  classRef: Newable<T>,
  groups: readonly Suggest<'*' | 'C' | 'U'>[] = ['*'],
  strictGroups: boolean = true,
  alwaysDefault: boolean = false,
): Promise<FormaSchema<T>> {
  const schema = { fields: {} } as FormaSchema<T>
  const metadata = getMetadataFor(classRef, groups, strictGroups, alwaysDefault)

  for (const fieldName in metadata as Record<keyof T, Metadata>) {
    const md = metadata[fieldName]

    if (md.type.isClassRef()) {
      const nestedClassRef = md.type.getAsClassRef()
      const nestedSchema = await getFormaSchemaOf(
        nestedClassRef,
        groups,
        strictGroups,
        alwaysDefault,
      )
      // @ts-expect-error TS2322
      //   - TS2322: `Type 'FormaSchema<unknown>' is not assignable to type 'T[Extract<keyof T, string>] extends object ? FormaSchema<T[Extract<keyof T, string>]> : FormaField<T[Extract<keyof T, string>]>'.`
      schema.fields[fieldName] = nestedSchema
    } else {
      const field: FormaField = {
        type: md.type.toString(),
        default: typeof md.default === 'function'
          ? md.default.constructor.name === 'AsyncFunction'
            ? await md.default()
            : md.default()
          : md.default,
        validationMetadata: [
          ...md.getValidationMetadataFor(groups, strictGroups, alwaysDefault),
        ],
      }
      // @ts-expect-error TS2322
      //   - TS2322: `Type 'FormaField<unknown>' is not assignable to type 'T[Extract<keyof T, string>] extends object ? FormaSchema<T[Extract<keyof T, string>]> : FormaField<T[Extract<keyof T, string>]>'.`
      schema.fields[fieldName] = field
    }
  }

  return schema
}
